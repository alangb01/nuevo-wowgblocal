<?php

use App\Http\Controllers\Auth\ConfirmPasswordController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/","WebController@index");
Route::get("como-jugar","WebController@comoJugar")->name("jugar");

//REGISTRO
Route::get("registrar","RegistrarController@registrarse")->name("registrarse");
Route::post("registrar","RegistrarController@store");
Route::get("activar/{username}","RegistrarController@activar")->name("activar");

//SOLICITUDES
Route::get("solicitar/usuario","SolicitarController@solicitarUsuario")->name("solicitar.usuario");
Route::post("solicitar/usuario","SolicitarController@procesarSolicitarUsuario");
Route::get("solicitar/cambio-clave","SolicitarController@solicitarCambioClave")->name("solicitar.cambio.clave");
Route::post("solicitar/cambio-clave","SolicitarController@procesarSolicitarCambioClave");
Route::get("cambiar-clave/{token}","CambioClaveController@cambiarClave")->name("cambiar.clave");
Route::post("cambiar-clave/{token}","CambioClaveController@procesarCambiarClave");

//INTRANET - CUENTA
Auth::routes(['register'=>false]);
Route::get('/home', 'HomeController@index')->name('home');
Route::prefix('cuenta')->name("cuenta.")->group(function () {
    Route::namespace('Cuenta')->middleware('auth')->group(function(){
        Route::get('mi-cuenta', 'CuentaController@index')->name('cuenta');
//        Route::get('mis-datos', 'InfoController@index')->name('info');
        Route::get('mis-personajes', 'PersonajesController@index')->name('personajes');
    });

});


//INTRANET - ADMINISTRACION
Route::prefix('intranet')->name("intranet.")->group(function () {

    Route::get('login', [LoginController::class, 'showAdminLoginForm'])->name('login');
    Route::post('login', [LoginController::class,'adminLogin']);
    Route::get('register', [RegisterController::class,'showAdminRegisterForm']);
    Route::post('register', [RegisterController::class,'createAdmin']);
    Route::get('password/confirm', [ConfirmPasswordController::class, 'showConfirmForm']);
    Route::post('password/confirm', [ConfirmPasswordController::class, 'confirm']);

    Route::get('password/reset', [ForgotPasswordController::class, 'showLinkRequestForm']);
    Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail']);
    Route::get('password/reset/{token}', [ResetPasswordController::class, 'showResetForm']);
    Route::post('password/reset', [ResetPasswordController::class, 'reset']);
//
    Route::namespace('Intranet')->middleware('auth:admin')->group(function(){
        Route::post('logout', [LoginController::class,'logout'])->name("logout");
        Route::get('/', [\App\Http\Controllers\Intranet\HomeController::class,'index'])->name('home');
        Route::get('cuentas', 'CuentasController@index')->name('cuentas');
        Route::get('cuenta/{username}/personajes', 'CuentasController@personajes')->name('cuentas.personajes');
        Route::get('cuenta/{username}/activar', 'CuentasController@activar')->name('cuentas.activar');
        Route::get('cuenta/{username}/editar', 'CuentasController@editar')->name('cuentas.editar');
        Route::put('cuenta/{username}/editar', 'CuentasController@update');

        Route::get('personajes', 'PersonajesController@index')->name('personajes');

        Route::get('items', 'ItemsController@index')->name('items');


        Route::get('correo/enviar', 'CorreoController@formulario')->name('correo');
        Route::post('correo/enviar', 'CorreoController@enviar')->name('correo');
        Route::get('correo/agregar-adjunto/{ID}', 'CorreoController@agregar')->name('correo.agregar');
        Route::get('correo/remover-adjunto/{ID}', 'CorreoController@remover')->name('correo.remover');
    });
});

