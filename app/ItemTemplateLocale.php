<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemTemplateLocale extends Model
{
    //

    protected $connection="mysql_world";

    protected $table='item_template_locale';
}
