<?php

namespace App;

use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;

class Servidor extends Model
{
    //
    protected $connection="mysql_auth";

    protected $table='realmlist';

    const ONLINE=true;
    const OFFLINE=false;

    public function getEstado(){
        try{
            $socket = @fsockopen($this->address,$this->port, $error_nro, $error_txt, (float)0.5);
            if($socket){
                @fclose($socket);
                return true;
            }
        }catch(Exception $e){
            //print_r($e);
        }
        return false;
    }

    public function getEstadoToString(){
        if($this->getEstado()){
            return "En linea";
        }else{
            return "Desconectado";
        }
    }

    public function getFechaHora(){
        return Carbon::now();
    }

    public static function comando($comando){
        try {
            $conn = new \SoapClient(NULL, array(
                'location' => 'http://127.0.0.1:7878/',
                'uri' => 'urn:TC',
                'style' => SOAP_RPC,
                'login' => env('SOAP_USER','charly'),
                'password' => env('SOAP_PASSWORD','030496e')
            ));
            $result = $conn->executeCommand(new \SoapParam($comando, 'command'));
            unset($conn);
        } catch (\Exception $e) {

            $result = "Have error on soap!\n";

            if (strpos($e, 'There is no such command') !== false) {
                $result = 'There is no such command!';
            }
        }

        return $result;
    }
}
