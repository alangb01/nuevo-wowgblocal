<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Crypt;

class Cuenta extends Authenticatable
{
    //
    protected $connection="mysql_auth";

    protected $table='account';

    public $timestamps = false;
    protected $dates=['joindate','last_login'];

    public function setearToken(){
        $this->token_key=bcrypt($this->username.":".Carbon::now());
    }

    public function scopeEncriptarClave($query,$usuario,$clave){
        $query->select(\DB::raw("SHA1(CONCAT(UPPER('".$usuario."'),':',UPPER('".$clave."'))) as pass"));
        $query->limit(1);

        $sha2=$query->first();

        return $sha2->pass;
    }

    public function personajes(){
        return $this->hasMany(Personaje::class,'account','id');
    }

    public function scopeFiltrar($query,$filtro){
        if($filtro!=""){
            $query->whereRaw('LOWER(username) like ?',['%'.strtolower($filtro).'%']);
            $query->orWhereRaw('LOWER(username) like ?',['%'.strtolower($filtro).'%']);
        }
        return $query;
    }

    public function desconectadoDesde(){
        $ahora=Carbon::now();
        $ultima=$this->last_login;
        $y=$ahora->diffInYears($ultima);
        $m=$ahora->diffInMonths($ultima);
        $d=$ahora->diffInDays($ultima);
        $h=$ahora->diffInHours($ultima);
        $i=$ahora->diffInMinutes($ultima);
        $s=$ahora->diffInSeconds($ultima);

        $cadena[]=($y>0?$y.' año':'').($y>1?'s':'')." ";
        $cadena[]=($m>0?$m.' mes':'').($m>1?'es':'')." ";
        $cadena[]=($d>0?$d.' dia':'').($d>1?'s':'')." ";

        $cadena[]=($h>0?$h.' hora':'').($h>1?'s':'')." ";
        $cadena[]=($i>0?$i.' minuto':'').($i>1?'s':'')." ";
        $cadena[]=($s>0?$s.' segundo':'').($s>1?'s':'')." ";

        $cadena_final="";
        foreach ($cadena as $pos=>$linea){

            if($linea!=" " && $cadena_final==""){
                $cadena_final.=" ".$linea;
            }

        }
        return "hace ".trim($cadena_final)."";
    }

    public function registradoDesde(){
        $ahora=Carbon::now();
        $ultima=$this->joindate;
        $y=$ahora->diffInYears($ultima);
        $m=$ahora->diffInMonths($ultima);
        $d=$ahora->diffInDays($ultima);
        $h=$ahora->diffInHours($ultima);
        $i=$ahora->diffInMinutes($ultima);
        $s=$ahora->diffInSeconds($ultima);

        $cadena[]=($y>0?$y.' año':'').($y>1?'s':'')." ";
        $cadena[]=($m>0?$m.' mes':'').($m>1?'es':'')." ";
        $cadena[]=($d>0?$d.' dia':'').($d>1?'s':'')." ";

        $cadena[]=($h>0?$h.' hora':'').($h>1?'s':'')." ";
        $cadena[]=($i>0?$i.' minuto':'').($i>1?'s':'')." ";
        $cadena[]=($s>0?$s.' segundo':'').($s>1?'s':'')." ";

        $cadena_final="";
        foreach ($cadena as $pos=>$linea){

            if($linea!=" " && $cadena_final==""){
                $cadena_final.=" ".$linea;
            }

        }
        return "hace ".trim($cadena_final)."";
    }

    // Its from Trinitycore/account-creator
    public function calculateSRP6Verifier($username, $password, $salt)
    {
        // algorithm constants
        $g = gmp_init(7);
        $N = gmp_init('894B645E89E1535BBDAD5B8B290650530801B18EBFBF5E8FAB3C82872A3E9BB7', 16);

        // calculate first hash
        $h1 = sha1(strtoupper($username . ':' . $password), TRUE);

        // calculate second hash
        if(get_config('server_core') == 5)
        {
            $h2 = sha1(strrev($salt) . $h1, TRUE);  // From haukw
        } else {
            $h2 = sha1($salt . $h1, TRUE);
        }

        // convert to integer (little-endian)
        $h2 = gmp_import($h2, 1, GMP_LSW_FIRST);

        // g^h2 mod N
        $verifier = gmp_powm($g, $h2, $N);

        // convert back to a byte array (little-endian)
        $verifier = gmp_export($verifier, 1, GMP_LSW_FIRST);

        // pad to 32 bytes, remember that zeros go on the end in little-endian!
        $verifier = str_pad($verifier, 32, chr(0), STR_PAD_RIGHT);

        // done!
        if(get_config('server_core') == 5)
        {
            return strrev($verifier);  // From haukw
        } else {
            return $verifier;
        }
    }

// Returns SRP6 parameters to register this username/password combination with
    public function getRegistrationData($username, $password)
    {
        // generate a random salt
        $salt = random_bytes(32);

        // calculate verifier using this salt
        $verifier = $this->calculateSRP6Verifier($username, $password, $salt);

        // done - this is what you put in the account table!
        if(get_config('server_core') == 5)
        {
            $salt = strtoupper(bin2hex($salt));         	// From haukw
            $verifier = strtoupper(bin2hex($verifier));     // From haukw
        }

        return array($salt, $verifier);
    }

//From TrinityCore/AOWOW
    function verifySRP6()
    {
        $g = gmp_init(7);
        $N = gmp_init('894B645E89E1535BBDAD5B8B290650530801B18EBFBF5E8FAB3C82872A3E9BB7', 16);
        $x = gmp_import(
            sha1($this->salt . sha1(strtoupper($this->username . ':' . $this->password), TRUE), TRUE),
            1,
            GMP_LSW_FIRST
        );
        $v = gmp_powm($g, $x, $N);
        return ($this->verifier === str_pad(gmp_export($v, 1, GMP_LSW_FIRST), 32, chr(0), STR_PAD_RIGHT));
    }

    public function getHashActivador(){
        return Crypt::encryptString($this->username);
    }


}
