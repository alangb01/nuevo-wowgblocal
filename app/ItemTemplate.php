<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemTemplate extends Model
{
    //
    protected $connection="mysql_world";

    protected $table='item_template';

    public function scopeFiltrar($query,$filtro){
        $query->addSelect('entry');
        $query->joinLocale($filtro);

        return $query;
    }

    public function scopeJoinLocale($query,$filtro){
        $query->addSelect('item_template_locale.name as nombre');
        $query->join('item_template_locale',function($join) use($filtro){
            $join->on('item_template_locale.ID','=','entry');
            $join->where('item_template_locale.locale','=','esMX');

           if($filtro!=""){
               $palabras_filtro=explode(" ",$filtro);
               foreach($palabras_filtro as $palabra_filtro){
                   $join->where('item_template_locale.name','like','%'.$palabra_filtro.'%');
               }
           }else{
               $join->whereNull('id');
           }

        });

        return $query;
    }

    public function scopeBuscarPorIds($query,$ids){
        $query->addSelect('entry');
        $query->addSelect('item_template_locale.name as nombre');
        $query->join('item_template_locale',function($join) use($ids){
            $join->on('item_template_locale.ID','=','entry');
            $join->where('item_template_locale.locale','=','esMX');

            if(isset($ids) && count($ids)>0){
                $join->whereIn('ID',$ids);
            }else{
                $join->whereNull("ID");
            }

        });
    }
}
