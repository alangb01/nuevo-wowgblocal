<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mapa extends Model
{
    //
    protected $connection='mysql';
    protected $fillable=[
        'id','nombre'
    ];
    protected $primary_key = null;
    public $incrementing = false;
    public $timestamps=false;
}
