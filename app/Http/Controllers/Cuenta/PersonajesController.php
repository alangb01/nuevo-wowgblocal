<?php

namespace App\Http\Controllers\Cuenta;

use App\Cuenta;
use App\Http\Controllers\Controller;
use App\Personaje;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PersonajesController extends Controller
{
    //
    public function index(Request  $request){
        $cuenta=Auth::user();

        if($cuenta instanceof Cuenta){
            $personajes=Personaje::where('account',$cuenta->id)->paginate(10);

            foreach($personajes as $personaje){

            }

            $data=compact('cuenta','personajes');
        }

        return view('cuenta.personajes',$data);
    }
}
