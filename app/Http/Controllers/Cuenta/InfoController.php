<?php

namespace App\Http\Controllers\Cuenta;

use App\Http\Controllers\Controller;
use App\Info;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InfoController extends Controller
{
    //
    public function index(){
        $cuenta=Auth::user();
        $info=Info::find($cuenta->id);

        $data=compact('info');

        return view('cuenta.info',$data);
    }
}
