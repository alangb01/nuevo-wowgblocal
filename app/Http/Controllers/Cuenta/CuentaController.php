<?php
namespace App\Http\Controllers\Cuenta;

use App\Cuenta;
use App\Http\Controllers\Controller;
use App\Personaje;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CuentaController extends Controller
{
    //


    public function index(){

        $cuenta=Auth::user();

        $data=compact('cuenta');

        return view('cuenta.cuenta',$data);
    }



    public function editar($username){
        $cuenta=Cuenta::where('username',$username)->first();
        if(!($cuenta instanceof Cuenta)){
            abort(404,"No encontrado");
        }

        $data=compact('cuenta');
        return view('cuenta.cuenta-editar',$data);
    }
}
