<?php

namespace App\Http\Controllers;

use App\Cuenta;
use App\Mail\CuentaEnviarEnlaceCambioClave;
use App\Servidor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CambioClaveController extends Controller
{

    public function cambiarClave(Request $request, $token){
        //muestra la interfaz para recuperacion de clave y envia link para recuperacion
        $items=\DB::connection('mysql')->table('password_resets')->where('token',$token)->get();

        if($items->count()==1) {
            $cuenta = Cuenta::where('email', $items->first()->email)->firstOrFail();
            $data=compact('cuenta');
        }else{
            $data=compact('');
        }


        return view('cambiar-clave',$data);
    }

    public function procesarCambiarClave(Request $request, $token){
        //muestra la interfaz para recuperacion de clave y envia link para recuperacion
        $items=\DB::connection('mysql')->table('password_resets')->where('token',$token)->get();
        $clave=$request->input('password');
        $confirmar_clave=$request->input('password_confirmation');

        if($confirmar_clave!=$clave){
            $status_tipo=false;
            $status_mensaje="Las claves no coinciden.";
        }else{
            if($items->count()==1) {
                $cuenta = Cuenta::where('email', $items->first()->email)->firstOrFail();

                Servidor::comando(".account set password ".$cuenta->username." ".$clave." ".$clave);

                \DB::table('password_resets')->where('token',$token)->delete();

                $status_tipo=true;
                $status_mensaje="Se ha realizado el cambio de clave con éxito.";
            }else{
                $status_tipo=false;
                $status_mensaje="No se pudo procesar el cambio de clave.";
            }
        }

        return redirect()->route('solicitar.cambio.clave')
            ->with([
                'status_tipo'=>$status_tipo,
                'status_mensaje'=>$status_mensaje
            ]);
    }
}
