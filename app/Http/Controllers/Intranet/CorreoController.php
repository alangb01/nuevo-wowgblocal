<?php

namespace App\Http\Controllers\Intranet;

use App\Http\Controllers\Controller;
use App\ItemTemplate;
use App\Servidor;
use Illuminate\Http\Request;

class CorreoController extends Controller
{
    //
    public function formulario(Request $request){
        $id_adjuntos=$request->session()->get('adjuntos');
        $adjuntos=ItemTemplate::buscarPorIds($id_adjuntos)->get();


        $data=compact('adjuntos');
        return view('intranet.correo',$data);
    }

    public function enviar(Request $request){
        $usuario=$request->input('username');
        $asunto=$request->input('asunto');
        $mensaje=$request->input('mensaje');
        $adjuntos=$request->input('adjuntos',[]);

        $items_adjuntos="";
        foreach($adjuntos as $item=>$cantidad){
            $items_adjuntos.=$item.":".$cantidad." ";
        }

        //.send items $charactername "asunto" "Gracias por esforzarte, has alcanzado el maximo nivel" 44413:1
        $comando='.send items '.$usuario.' "'.$asunto.'" "'.$mensaje.'" '.trim($items_adjuntos);


        Servidor::comando($comando);

        $request->session()->forget('adjuntos');

        return redirect()->back();
    }

    public function agregar(Request $request, $id){
        $request->session()->push('adjuntos', $id);

        return redirect()->back();
    }

    public function remover(Request $request, $id){
        $session=collect($request->session()->get('adjuntos'));

        $key =  $session->search($id);

        $request->session()->forget('adjuntos.'.$key);
        return redirect()->back();
    }
}
