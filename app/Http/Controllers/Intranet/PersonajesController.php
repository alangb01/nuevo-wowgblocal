<?php

namespace App\Http\Controllers\Intranet;

use App\Cuenta;
use App\Http\Controllers\Controller;
use App\Personaje;
use Illuminate\Http\Request;

class PersonajesController extends Controller
{
    //
    public function index(Request  $request){
        $filtro=$request->get('filtro');
        $personajes=Personaje::filtrar($filtro)->paginate(10)->appends(compact('filtro'));

        foreach($personajes as $personaje){

        }

        $data=compact('personajes','filtro');
        return view('intranet.personajes',$data);
    }
}
