<?php

namespace App\Http\Controllers\Intranet;

use App\Http\Controllers\Controller;
use App\ItemTemplate;
use Illuminate\Http\Request;

class ItemsController extends Controller
{
    //

    public function index(Request $request){
        $filtro=$request->get('filtro');
        $items=ItemTemplate::filtrar($filtro)->paginate(10)->appends(compact('filtro'));



        $data=compact('items','filtro');
        return view('intranet.items',$data);
    }
}
