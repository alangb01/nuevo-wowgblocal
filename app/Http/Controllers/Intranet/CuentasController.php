<?php

namespace App\Http\Controllers\Intranet;

use App\Cuenta;
use App\Http\Controllers\Controller;
use App\Personaje;
use Illuminate\Http\Request;

class CuentasController extends Controller
{
    //
    public function index(Request  $request){
        $filtro=$request->get('filtro');

        $cuentas=Cuenta::filtrar($filtro)->paginate(10)->appends(compact('filtro'));

        foreach($cuentas as $cuenta){
            $cuenta->url_personajes=route('intranet.cuentas.personajes',[$cuenta->username]);
            $cuenta->url_activar=route('intranet.cuentas.activar',[$cuenta->username]);
            $cuenta->url_editar=route('intranet.cuentas.editar',[$cuenta->username]);
        }

        $data=compact('cuentas','filtro');
        return view('intranet.cuentas',$data);
    }

    public function personajes(Request $request, $username){

        $cuenta=Cuenta::where('username',$username)->first();

        if($cuenta instanceof Cuenta){
            $personajes=Personaje::where('account',$cuenta->id)->paginate(10);

            foreach($personajes as $personaje){

            }

            $data=compact('cuenta','personajes');
        }

        return view('intranet.cuentas-personajes',$data);
    }

    public function activar($username){
        $cuenta=Cuenta::where('username',$username)->first();

        if($cuenta instanceof Cuenta){
            $cuenta->locked=0;
            $cuenta->save();

            $status_tipo=true;
            $status_mensaje='La cuenta se actualizó con éxito.';
        }

        return redirect()->back()->with([
            'status_tipo'=>$status_tipo,
            'status_mensaje'=>$status_mensaje
        ]);
    }

    public function editar($username){
        $cuenta=Cuenta::where('username',$username)->first();
        if(!($cuenta instanceof Cuenta)){
            abort(404,"No encontrado");
        }

        $data=compact('cuenta');
        return view('intranet.cuenta-editar',$data);
    }

    public function update(Request $request, $username){
        $this->validate($request,[
           'username'=>'required',
           'email'=>'required'
        ]);

        $cuenta=Cuenta::where('username',$username)->first();
        if($cuenta instanceof Cuenta){
            $cuenta->username=strtoupper($request->input('username'));
            $cuenta->email=$request->input('email');
            $cuenta->save();

            $status_tipo=true;
            $status_mensaje='La cuenta se actualizó con éxito.';

        }else{
            $status_tipo=false;
            $status_mensaje='La cuenta no se pudo actualizar.';

        }
        return redirect()->back()->with([
            'status_tipo'=>$status_tipo,
            'status_mensaje'=>$status_mensaje
        ]);
    }
}
