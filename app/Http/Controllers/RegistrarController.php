<?php

namespace App\Http\Controllers;

use App\Cuenta;
use App\Mail\CuentaNuevoRegistro;
use App\Servidor;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class RegistrarController extends Controller
{
    //
    public function registrarse(){
        //muestra la interfaz de registro de cuenta
        return view("registrarse");
    }


    public function store(Request $request){
        //crea la cuenta bloqueada y envia correo para activacion
        $this->validate($request,[
            'username'=>'required|unique:mysql_auth.account',
            'email'=>'required|unique:mysql_auth.account',
            'password'=>'required|min:6',
            'password_confirmation'=>'required|same:password',
        ]);

        $usuario=$request->input("username");
        $clave=$request->input('password');

        Servidor::comando(".account create ".$usuario." ".$clave);

        $cuenta=Cuenta::where('username',$usuario)->first();

        if($cuenta instanceof Cuenta){
            $cuenta->email=$request->input('email');
            $cuenta->locked=1;
            $cuenta->save();

            $status_tipo=true;
            $status_mensaje='La cuenta se registro con éxito';

            Mail::to($cuenta->email)
                ->send(new CuentaNuevoRegistro($cuenta));

        }else{
            $status_tipo=false;
            $status_mensaje='La cuenta no se pudo registrar.';
        }

        return redirect()->back()
            ->with([
                'status_tipo'=>$status_tipo,
                'status_mensaje'=>$status_mensaje
            ]);
    }

    public function activar($hash){
        try {

            $username = Crypt::decryptString($hash);


            $cuenta=Cuenta::where('username',$username)->firstOrFail();

            if($cuenta->locked==1){
                $cuenta->locked=0;
                $cuenta->save();
                $mensaje="La cuenta ha sido activada";
            }else{
                $mensaje="La cuenta se encuentra activa";
            }
        } catch (DecryptException $e) {
            $mensaje="El codigo de activacion es incorrecto.";
        }



        $data=compact('mensaje','cuenta');
        return view('activar-cuenta',$data);
    }
}
