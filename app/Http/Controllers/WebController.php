<?php

namespace App\Http\Controllers;

use App\Cuenta;
use App\Mail\CuentaEnviarEnlaceCambioClave;
use App\Mail\CuentaNuevoRegistro;
use App\Mail\NotificacionAdministrador;
use App\Personaje;
use App\Servidor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class WebController extends Controller
{
    //
    public function index(Request $request){
        $personajes_online=Personaje::online($request->get('nombre_online'))
                            ->paginate(10,['*'],'pagina_online');
        $personajes_top=Personaje::top($request->get('nombre_top'))
                            ->paginate(10,['*'],'pagina_top');

        $personajes_ultima_conexion=Personaje::ultimosConectados($request->get('nombre_top'))
            ->paginate(10,['*'],'pagina_recien_conectados');

        $servidores=Servidor::all();


        $data=compact("servidores","personajes_online","personajes_top","personajes_ultima_conexion");
        return view('inicio',$data);
    }

    public function comoJugar(){
//        $archivos = Storage::disk('s3')->allFiles("recursos");
//        $data = compact('archivos');
        $data=[];
        return view('como-jugar', $data);
    }
}
