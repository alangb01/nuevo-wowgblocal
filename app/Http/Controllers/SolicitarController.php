<?php

namespace App\Http\Controllers;

use App\Cuenta;
use App\Mail\CuentaEnviarEnlaceCambioClave;
use App\Mail\CuentaEnviarUsuario;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SolicitarController extends Controller
{
    //
    public function solicitarUsuario(){
        //muestra la interfaz para recuperacion de clave y envia link para recuperacion

        return view('solicitar-usuario');
    }

    public function procesarSolicitarUsuario(Request  $request){
        $email=$request->input('email');

        $cuenta=Cuenta::where('email',$email)->first();

        if($cuenta instanceof Cuenta){
            Mail::to($cuenta->email)
                ->send(new CuentaEnviarUsuario($cuenta));

            $status_tipo=true;
            $status_mensaje="Se ha enviado un correo al email registrado, por favor reviselo para continuar.";
        }else{
            $status_tipo=false;
            $status_mensaje="No se ha podido procesar su solicitud.";
        }

        return redirect()->back()
            ->with([
                'status_tipo'=>$status_tipo,
                'status_mensaje'=>$status_mensaje
            ]);
    }

    //
    public function solicitarCambioClave(){
        //muestra la interfaz para recuperacion de clave y envia link para recuperacion

        return view('solicitar-cambio');
    }

    public function procesarSolicitarCambioClave(Request  $request){
        $email=$request->input('email');

        $cuenta=Cuenta::where('email',$email)->first();

        if($cuenta instanceof Cuenta){
            $token=\Str::random(60);

            \DB::table('password_resets')->insert([
                'email' => $cuenta->email,
                'token' => $token,
                'created_at' => Carbon::now(),
            ]);

            Mail::to($cuenta->email)
                ->send(new CuentaEnviarEnlaceCambioClave($cuenta,$token));

            $status_tipo=true;
            $status_mensaje="Se ha enviado un correo al email registrado, por favor reviselo para continuar.";
        }else{
            $status_tipo=false;
            $status_mensaje="No se ha podido procesar su solicitud.";
        }

        return redirect()->back()
            ->with([
                'status_tipo'=>$status_tipo,
                'status_mensaje'=>$status_mensaje
            ]);
    }
}
