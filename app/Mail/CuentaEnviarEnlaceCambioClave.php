<?php

namespace App\Mail;

use App\Cuenta;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CuentaEnviarEnlaceCambioClave extends Mailable
{
    use Queueable, SerializesModels;

    public $cuenta;
    public $token;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Cuenta $cuenta,$token)
    {
        //
        $this->cuenta=$cuenta;
        $this->token=$token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
                ->markdown('emails.cuenta.token')
                ->subject("Solicitud de cambio de clave");
    }
}
