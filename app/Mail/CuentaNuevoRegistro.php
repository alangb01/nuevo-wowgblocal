<?php

namespace App\Mail;

use App\Cuenta;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CuentaNuevoRegistro extends Mailable
{
    use Queueable, SerializesModels;

    public $cuenta;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Cuenta $cuenta)
    {
        //
        $this->cuenta=$cuenta;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
//        dd($this->cuenta->email);
        return $this->markdown('emails.cuenta.nueva')
            ->subject("Bienvenido a ".env("APP_NAME"));
    }
}
