<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Personaje extends Model
{
    //
    protected $connection="mysql_characters";

    protected $table='characters';

    protected $dates=['logout_time'];

    const ALIANZA=1;
    const HORDA=2;

    const HOMBRE=0;
    const MUJER=1;

    const RAZA_HUMANO=1;
    const RAZA_ORCO=2;
    const RAZA_ENANO=3;
    const RAZA_ELFO_NOCHE=4;
    const RAZA_NO_MUERTO=5;
    const RAZA_TAUREN=6;
    const RAZA_GNOMO=7;
    const RAZA_TROLL=8;
    const RAZA_ELFO_SANGRE=10;
    const RAZA_DRAENEI=11;

    const CLASE_GUERRERO=1;
    const CLASE_PALADIN=2;
    const CLASE_CAZADOR=3;
    const CLASE_PICARO=4;
    const CLASE_SACERDORTE=5;
    const CLASE_CABALLERO_MUERTE=6;
    const CLASE_SHAMAN=7;
    const CLASE_MAGO=8;
    const CLASE_BRUJO=9;
    const CLASE_DRUIDA=11;

    private static  $clases_alianza=array(self::RAZA_HUMANO,
        self::RAZA_ENANO,
        self::RAZA_GNOMO,
        self::RAZA_ELFO_NOCHE,
        self::RAZA_DRAENEI);

    private static $clases_horda=array(self::RAZA_ORCO,
        self::RAZA_TAUREN,
        self::RAZA_TROLL,
        self::RAZA_NO_MUERTO,
        self::RAZA_ELFO_SANGRE);

    public function cuenta(){
        return $this->belongsTo(Cuenta::class,'account','id');
    }

    public function getOro(){
        return floor($this->money/(100*100));
    }

    public function getPlata(){
        return floor(($this->money-$this->getOro()*100*100)/100);
    }

    public function getBronce(){
        return $this->money%100;
    }

    public function getImagenRaza(){
        return asset("img/iconos/r".$this->race."-".$this->gender.".gif");
    }

    public function getImagenClase(){
        return asset("img/iconos/c".$this->class.".gif");
    }

    public function getBando(){
        if(in_array($this->race,self::$clases_alianza)){
            $bando=self::ALIANZA;
        }elseif(in_array($this->race,self::$clases_horda)){
            $bando=self::HORDA;
        }else{
            $bando=0;
        }

        return $bando;
    }

    public function getImagenBando(){
        return asset("img/iconos/b".$this->getBando().".gif");
    }

    public function scopeTop($query,$filtrar){
        $query->select("characters.*");

        $query->filtrar($filtrar);

        $query->orderBy("totalKills","desc")->orderBy("totaltime","desc");
        //order by totalKills DESC, totaltime desc

        return $query;
    }

    public function scopeCargarUbicacion($query){
        $query->select("characters.*");

        $query->addSelect(\DB::raw("CASE
            WHEN z.nombre<>'' THEN z.nombre
            WHEN m.nombre<>'' THEN m.nombre
            ELSE 'desconocida' END AS location"));

        $query->leftJoin(env("DB_DATABASE").".mapas as m","m.id","=","characters.map");
        $query->leftJoin(env("DB_DATABASE").".zonas as z","z.id","=","characters.map");

        return $query;
    }

    public function scopeOnline($query,$filtrar){
        $query->cargarUbicacion();

        $query->where("online",true);

        $query->filtrar($filtrar);

        return $query;
    }

    public function scopeUltimosConectados($query,$filtrar){
        $query->cargarUbicacion();

        $query->where("online",false);
        $query->orderBy("logout_time","desc");

        $query->filtrar($filtrar);

        return $query;
    }

    public function scopeFiltrar($query,$filtrar){
        if($filtrar!=""){
            $query->whereRaw('LOWER(name) like ?',['%'.strtolower($filtrar).'%']);
        }

        return $query;
    }

    public function desconectadoDesde(){
        $ahora=Carbon::now();
        $ultima=$this->logout_time;
        $y=$ahora->diffInYears($ultima);
        $m=$ahora->diffInMonths($ultima);
        $d=$ahora->diffInDays($ultima);
        $h=$ahora->diffInHours($ultima);
        $i=$ahora->diffInMinutes($ultima);
        $s=$ahora->diffInSeconds($ultima);

        $cadena[]=($y>0?$y.' año':'').($y>1?'s':'')." ";
        $cadena[]=($m>0?$m.' mes':'').($m>1?'es':'')." ";
        $cadena[]=($d>0?$d.' dia':'').($d>1?'s':'')." ";

        $cadena[]=($h>0?$h.' hora':'').($h>1?'s':'')." ";
        $cadena[]=($i>0?$i.' minuto':'').($i>1?'s':'')." ";
        $cadena[]=($s>0?$s.' segundo':'').($s>1?'s':'')." ";

        $cadena_final="";
        foreach ($cadena as $pos=>$linea){

            if($linea!=" " && $cadena_final==""){
                $cadena_final.=" ".$linea;
            }

        }
        return "hace ".trim($cadena_final)."";
    }

    public function getSetId(){
        // .send items $charactername "asunto" "Gracias por esforzarte, has alcanzado el maximo nivel" 44413:1 // moto
    }

    public function getRaceDBC(){
        return pow(2,$this->race-1);
    }

    public function getClassDBC(){
        return pow(2,$this->class-1);
    }
}
