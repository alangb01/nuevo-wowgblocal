<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
    //
    protected $connection="mysql";

    protected $table='info';

    protected $id='id';
    public $timestamps = false;
    protected $dates=['nacimiento'];
}
