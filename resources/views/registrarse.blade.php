@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-6 offset-md-3 mt-2">
            <div class="card bg-dark text-white" >
                <div class="card-body">
                    <h5 class="card-title text-center">Registrar una cuenta</h5>
                    @if(session()->exists("status_mensaje"))
                        @if(session()->get("status_tipo")===true)
                            <div class="alert alert-success">{{ session()->get("status_mensaje") }}</div>
                        @elseif(session()->get("status_tipo")===false)
                            <div class="alert alert-danger">{{ session()->get("status_mensaje") }}</div>
                        @endif
                    @endif
                    <form class="form-horizontal" method="POST" action="">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="inputUsuario">Usuario</label>
                            <input type="text" name="username" id="inputUsuario" aria-describedby="usuario"
                                   class="form-control @error('username') is-invalid @enderror" value="{{ old('username') }}"
                                placeholder="Ingresar su usuario">
    {{--                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                            @error('username')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputEmail">Email</label>
                            <input type="text" name="email" id="inputEmail" aria-describedby="email"
                                   class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}"
                                placeholder="Ingresar su email">
                            {{--                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                            @error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputPassword">Password</label>
                            <input type="password" name="password" id="inputPassword" aria-describedby="password"
                                   class="form-control @error('password') is-invalid @enderror"
                                placeholder="Ingresar su password">
                            {{--                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                            @error('password')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputConfirmarPassword">Confirmar password</label>
                            <input type="password" name="password_confirmation" id="inputConfirmarPassword" aria-describedby="confirmar password"
                                   class="form-control @error('password_confirmation') is-invalid @enderror" placeholder="Re-ingresar el mismo password">
                            {{--                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                            @error('password_confirmation')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary float-right">
                            Crear cuenta
                        </button>
                    </form>
    {{--                <a href="#" class="btn btn-primary">Go somewhere</a>--}}
                </div>
            </div>
        </div>
    </div>
@endsection
