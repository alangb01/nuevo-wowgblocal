@extends('layouts.app')

@section('content')
        <div class="row">
            <div class="col-md-6 offset-md-3 mt-2">
                <div class="card bg-dark">
                    <div class="card-header">
                        <h4 class="card-title mb-0">Estado de activacion</h4>
                    </div>
                    <div class="card-body text-white">
                       <p>{{$mensaje}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
