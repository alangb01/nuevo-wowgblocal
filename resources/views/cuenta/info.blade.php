@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-3">
                <div class="card bg-dark text-white" >
                    <div class="card-body">
                        <h5 class="card-title text-center">Información personal</h5>
                        @if(session()->exists("status_mensaje"))
                            @if(session()->get("status_tipo")===true)
                                <div class="alert alert-success">{{ session()->get("status_mensaje") }}</div>
                            @elseif(session()->get("status_tipo")===false)
                                <div class="alert alert-danger">{{ session()->get("status_mensaje") }}</div>
                            @endif
                        @endif
                        <form class="form-horizontal" method="POST" action="">
                            {{ csrf_field() }}
                            {{ method_field('put') }}
                            <div class="form-group">
                                <label for="inputUsuario">Nombre</label>
                                <input type="text" name="username" id="inputUsuario" aria-describedby="usuario"
                                       class="form-control @error('username') is-invalid @enderror" value="{{ old('username',$info->nombres) }}">
                                {{--                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                                @error('username')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="inputEmail">Apellidos</label>
                                <input type="text" name="email" id="inputEmail" aria-describedby="email"
                                       class="form-control @error('email') is-invalid @enderror" value="{{ old('email',$info->apellidos) }}">
                                {{--                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                                @error('email')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="inputEmail">Nacimiento</label>
                                <input type="text" name="email" id="inputEmail" aria-describedby="email"
                                       class="form-control @error('email') is-invalid @enderror" value="{{ old('email',$info->nacimiento->format("Y-m-d")) }}">
                                {{--                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                                @error('email')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>



                            {{--                            <button type="submit" class="btn btn-primary float-right">--}}
                            {{--                                Actualizar cuenta--}}
                            {{--                            </button>--}}
                        </form>
                        {{--                <a href="#" class="btn btn-primary">Go somewhere</a>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
