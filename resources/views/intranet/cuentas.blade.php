@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card bg-dark">
                    <div class="card-header">
                        <form class="form-inline my-lg-0 ">
                            <h4 class="card-title col-md-6 mb-0">Cuentas registradas</h4>
                            <div class="col-md-6 ">
                                <input name="filtro" class="form-control mr-sm-2"
                                       type="search" placeholder="Ingrese un filtro" aria-label="Search"
                                       value="{{ $filtro }}"
                                >
                                <button class="btn btn-success my-2 my-sm-0" type="submit">
                                    <i class="fas fa-search"></i> Filtrar
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="card-body text-white">
                        <table class="table table-hover table-bordered table-dark table-sm mb-0">
                            <thead>
                            <tr>
                                <th class="text-center">Nro</th>
                                <th class="text-center">Cuenta / Email</th>
                                <th class="text-center">Ultima conexión</th>
                                <th class="text-center">Registro</th>
                                <th class="text-center">Conexion</th>
                                <th class="text-center">Bloqueado</th>
                                <th class="text-center">Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($cuentas as $posicion=>$cuenta)
                                <tr >
                                    <td class="text-center">{{ $posicion+1+$cuentas->perPage()*($cuentas->currentPage()-1) }}</td>
                                    <td class="text-center">{{ $cuenta->username  }}<br>{{ $cuenta->email }}</td>
                                    <td class="text-center">
                                        {{ $cuenta->last_login }} <br>
                                        {{ $cuenta->desconectadoDesde() }}
                                    </td>
                                    <td class="text-center">
                                        {{ $cuenta->joindate }}
                                        <br>
                                        {{ $cuenta->registradoDesde() }}
                                    </td>
                                    <td class="text-center">
                                        @if($cuenta->online==1)
                                            <a class="btn btn-success btn-sm col-12 ">
                                                <i class="fas fa-thumbs-up "></i> Conectado
                                            </a>
                                        @else
                                            <a class="btn btn-danger btn-sm col-12">
                                                <i class="fas fa-thumbs-down "></i> Desconectado
                                            </a>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if($cuenta->locked==0)
                                            <a class="btn btn-success btn-sm col-12">
                                                <i class="fas fa-check "></i> Activada
                                            </a>
                                        @else
                                            <a href="{{ $cuenta->url_activar }}" class="btn btn-danger btn-sm col-12">
                                                <i class="fas fa-ban"></i> Por confirmar
                                            </a>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ $cuenta->url_editar }}" class="btn btn-primary btn-sm">Editar cuenta</a>
                                        <a href="{{ $cuenta->url_personajes }}" class="btn btn-primary btn-sm">
                                            Ver personajes
                                        </a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="9">No hay cuentas disponibles</td>
                                </tr>
                            @endforelse
                            </tbody>
                            <tfoot>
                            @if($cuentas->hasPages())
                                <tr>
                                    <td class="text-center" colspan="9">{{ $cuentas->links() }}</td>
                                </tr>
                            @endif
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
