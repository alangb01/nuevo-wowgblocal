@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="card bg-dark">
                    <div class="card-header">
                        <h4 class="card-title mb-0">Personajes de {{ $cuenta->username }}</h4>
                    </div>
                    <div class="card-body text-white">
                        <table class="table table-bordered table-dark table-sm mb-0">
                            <thead>
                            <tr>
                                <th class="text-center">Nro</th>
                                <th colspan="3" class="text-center">Personajes</th>
                                <th class="text-center">Victorias</th>
                                <th class="text-center">Oro</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($personajes as $posicion=>$personaje)
                                <tr class="{{ $personaje->getBando()==\App\Personaje::ALIANZA?'alianza':'horda' }}">
                                    <td class="text-center">{{ $posicion+1+$personajes->perPage()*($personajes->currentPage()-1) }}</td>
                                    <td class="text-center">{{ $personaje->name  }}</td>
                                    <td class="text-center">{{ $personaje->level }}</td>
                                    <td class="text-center">
                                        <img src="{{ $personaje->getImagenBando() }}" alt="">
                                        <img src="{{ $personaje->getImagenClase() }}" alt="">
                                        <img src="{{ $personaje->getImagenRaza() }}" alt="">
                                    </td>
                                    <td class="text-right">{{ $personaje->totalKills }}</td>
                                    <td class="text-right">
                                        {{ $personaje->getOro() }} <i class="fas fa-coins text-warning"></i>
                                        {{ $personaje->getPlata() }} <i class="fas fa-coins"></i>
                                        {{ $personaje->getBronce() }} <i class="fas fa-coins text-danger"></i>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5">No hay personajes disponibles</td>
                                </tr>
                            @endforelse
                            </tbody>
                            <tfoot>
                            @if($personajes->hasPages())
                                <tr>
                                    <td class="text-center" colspan="5">{{ $personajes->links('vendor.pagination.simple-minimo') }}</td>
                                </tr>
                            @endif
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
