@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 mt-2 offset-3">
                <div class="card bg-dark text-white" >
                    <div class="card-body">
                        <h5 class="card-title text-center">Enviar regalos por correo</h5>
                        @if(session()->exists("status_mensaje"))
                            @if(session()->get("status_tipo")===true)
                                <div class="alert alert-success">{{ session()->get("status_mensaje") }}</div>
                            @elseif(session()->get("status_tipo")===false)
                                <div class="alert alert-danger">{{ session()->get("status_mensaje") }}</div>
                            @endif
                        @endif
                        <form class="form-horizontal" method="POST" action="">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="inputUsuario">Usuario</label>
                                <input type="text" name="username" id="inputUsuario" aria-describedby="usuario"
                                       class="form-control @error('username') is-invalid @enderror" value="{{ old('username') }}">
                                {{--                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                                @error('username')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="inputAsunto">Asunto</label>
                                <input type="text" name="asunto" id="inputAsunto" aria-describedby="asunto"
                                       class="form-control @error('asunto') is-invalid @enderror" value="{{ old('asunto','Regalo por tu logro') }}">
                                {{--                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                                @error('asunto')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="inputMensaje">Mensaje</label>

                                {{--                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                                <textarea name="mensaje" id="inputMensaje" class="form-control @error('mensaje') is-invalid @enderror" aria-describedby="usuario">{{ old('mensaje','Por tu esfuerzo en el juego has ganado las siguientes recompensas, felicitaciones') }}</textarea>
                                @error('mensaje')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            @if(isset($adjuntos) && $adjuntos->count()>0)
                                <div class="form-group">
                                    <label for="inputMensaje">Adjuntos</label>
                                    <table class="table table-bordered table-sm">
                                        <tr>
                                            <th class="bg-white">Item</th>
                                            <th class="bg-white">Cantidad</th>
                                        </tr>
                                        @foreach($adjuntos as $item)
                                            <tr>
                                                <td class="text-white">{{ $item->nombre }}</td>
                                                <td>
                                                    <input type="text" name="adjuntos[{{ $item->entry }}]" value="1" class="form-control-sm">
                                                    <a href="{{ route('intranet.correo.remover',$item->entry) }}" class="btn btn-danger btn-sm">Remover</a>
                                                </td>
                                            </tr>
                                            @endforeach
                                    </table>

                                </div>
                            @endif

                            <button type="submit" class="btn btn-primary float-right">
                                Enviar
                            </button>
                        </form>
                        {{--                <a href="#" class="btn btn-primary">Go somewhere</a>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
