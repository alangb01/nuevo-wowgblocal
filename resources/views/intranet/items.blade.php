@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 mt-2 offset-md-2">
                <div class="card bg-dark">
                    <div class="card-header">
                        <h4 class="card-title mb-0">Items del Juego</h4>
                    </div>
                    <div class="card-body text-white">
                        <form action="" class="form ">
                            <input type="text" name="filtro" value="{{old('filtro',$filtro)}}" class="form-control">
                        </form>
                        <table class="table table-bordered table-dark table-sm mb-0">
                            <thead>
                            <tr>
                                <th class="text-center">Entry</th>
                                <th colspan="3" class="text-center">Nombre</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($items as $posicion=>$item)
                                <tr >
                                    <td class="text-center">{{ $item->entry  }}</td>
                                    <td class="text-center">{{ $item->nombre }}</td>
                                    <td class="text-center" >
                                        <a href="{{ route('intranet.correo.agregar',$item->entry) }}" class="btn btn-outline-success btn-sm">Agregar</a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5">No hay items disponibles</td>
                                </tr>
                            @endforelse
                            </tbody>
                            <tfoot>
                            @if($items->hasPages())
                                <tr>
                                    <td class="text-center" colspan="5">{{ $items->links('vendor.pagination.simple-minimo') }}</td>
                                </tr>
                            @endif
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
