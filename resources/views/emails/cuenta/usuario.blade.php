@component('mail::message')
# Solicitud de usuario

Su usuario para iniciar sesión es: {{ $cuenta->username }}

@endcomponent
