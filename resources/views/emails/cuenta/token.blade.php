@component('mail::message')
# Solicitud de cambio de clave

Para continuar con el cambio de clave ingrese al siguiente enlace.

@component('mail::button', ['url' => route("cambiar.clave",['token'=>$token])])
Formulario para cambio de clave
@endcomponent

@endcomponent
