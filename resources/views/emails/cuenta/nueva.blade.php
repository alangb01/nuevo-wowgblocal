@component('mail::message')
# BIENVENIDO A {{ env('APP_NAME') }}

Por favor, para poder ingresar al juego, primero activa tu cuenta en el siguiente enlace.
@component('mail::button',['url'=>route('activar',[$cuenta->getHashActivador()])])
    Activar mi cuenta
@endcomponent

Si aun no tienes el juego, puedes instalarlo siguiendo estas indicaciones de
@component('mail::button', ['url' => route("jugar")])
    Como jugar
@endcomponent

Para ver a tus personajes en el ranking o ver los personajes conectados al mundo de warcraft puedes visitar nuestro sitio.
@component('mail::button', ['url' => env('APP_URL')])
    Sitio web
@endcomponent

Tambien puedes visitar nuestra fanpage, donde colocaremos publicaciones, fotos y videos sobre actividades dentro y fuera del servidor.
@component('mail::button', ['url' => "https://facebook.com/wowgblocal"])
    Fanpage
@endcomponent

Gracias por registrarte en nuestro sitio, esperamos disfrutes del juego.


@endcomponent
