@extends("layouts.app")
@section("content")
    <div class="row ">
        <div class="col-lg-4 col-sm-12 mt-2 order-lg-1 order-md-2 order-2 ">
            @include('_inicio.ultimos-conectados')
        </div>
        <div class="col-lg-4 col-sm-12 mt-2 order-lg-2 order-md-1 order-1">
            <div class="card bg-dark">
                <div class="card-body text-white">
                    <h5 class="card-title font-weight-bold text-center">Configurar servidor</h5>
                    <table class="table table-bordered table-dark table-sm mb-0">
                        @forelse($servidores as $posicion=>$servidor)
                            <tr>
                                <td class="text-center">{{ $servidor->address }}<br>
                                    {{--<a href="#" data-toggle="modal" data-target="#configurarServidor">Configurar</a>--}}
                                </td>
                                <td class="vertical-medio text-center {{ $servidor->getEstado()==App\Servidor::ONLINE?'bg-success':'bg-danger' }}">
                                    {{ $servidor->getEstadoToString() }}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td>No hay servidores disponibles</td>
                            </tr>
                        @endforelse
                    </table>
                </div>
            </div>
            <div class="card bg-dark mt-2">
                <div class="card-body text-white">
                    <h5 class="card-title font-weight-bold text-center">Configurar servidor</h5>
                    <table class="table table-bordered table-dark table-sm mb-0">
                        <thead>
                        <tr>
                            <th colspan="3"  class="text-center">Personajes</th>
                            <th class="text-center">Ubicación</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($personajes_online as $posicion=>$personaje)
                            <tr class="{{ $personaje->getBando()==\App\Personaje::ALIANZA?'alianza':'horda' }}">
                                <td class="text-center">{{ $personaje->name  }}</td>
                                <td class="text-center">{{ $personaje->level }}</td>
                                <td class="text-center">
                                    <img src="{{ $personaje->getImagenBando() }}" alt="">
                                    <img src="{{ $personaje->getImagenClase() }}" alt="">
                                    <img src="{{ $personaje->getImagenRaza() }}" alt="">
                                </td>
                                <td class="text-center">{{ $personaje->location }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">No hay personajes en linea</td>
                            </tr>
                        @endforelse
                        </tbody>
                        <tfoot>
                        @if($personajes_online->hasPages())
                            <tr>
                                <td class="text-center" colspan="5">{{ $personajes_online->links('vendor.pagination.simple-minimo') }}</td>
                            </tr>
                        @endif
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-12 mt-2 order-lg-3 order-md-3 order-3">
            @include('_inicio.ranking')
        </div>
    </div>

@endsection
