<div class="card bg-dark">
    <div class="card-header">
        <h4 class="card-title mb-0 text-center">Configurar Discord</h4>
    </div>
    <div class="card-body text-white">
        <p class="card-text">Instala la aplicación y comunicate con la comunidad: </p>
        <ol>
            <li>
                Descarga el instalador para tu sistema operativo <a href="https://discord.com/download" class="btn btn-outline-primary btn-sm" target="_blank">aquí</a>
            </li>
            <li>
                Conectate con el siguiente enlace de <a href="https://discord.gg/H8yVvJ5K3n" class="btn btn-outline-primary btn-sm" target="_blank">invitación</a>
            </li>

        </ol>
    </div>
</div>
