<div class="card bg-dark">
    <div class="card-header">
        <h4 class="card-title mb-0 text-center">Como instalar</h4>
    </div>
    <div class="card-body text-white">
        <p class="card-text ">Pasos para realizar la instalación del juego.</p>
        <ol>

            <li>Descarga todos los archivos en la misma carpeta. <br>
                <span>
                    <a href="https://drive.google.com/drive/folders/1y5Rcji3RcJObW04V_DQOmW6AJXoWE28f?usp=sharing" class="btn btn-outline-primary btn-sm" target="_blank"> Google Drive </a>
                    <a href="https://mega.nz/folder/IjQw2TjT#PovRtIGZtWvoi96WQNpEng" class="btn btn-outline-danger btn-sm" target="_blank">MEGA.NZ</a>
                </span>
            </li>
            <li>Ejecuta el archivo "gblocal.part01.rar" (password: wowgblocal@)</li>
            <li>Selecciona la ubicación donde se va a instalar.</li>
            <li>Abre el archivo "jugar-wowgblocal.exe" y presiona SI<br/>(Necesitaras tener instalado el netframework 4.0)</li>
            <li>Ingresa tu usuario y clave para conectarte.</li>
        </ol>
    </div>
</div>
