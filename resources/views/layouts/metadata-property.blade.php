<!-- MS, fb & Whatsapp -->

<!-- MS Tile - for Microsoft apps-->
{{--<meta name="msapplication-TileImage" content="http://www.example.com/image01.jpg">--}}

<!-- fb & Whatsapp -->

<!-- Site Name, Title, and Description to be displayed -->
{{--<meta property="og:site_name" content="">--}}
<meta property="og:title" content="{{ env('APP_NAME') }}">
<meta property="og:description" content="Servidor wowgblocal v3.3.5a, Registrate y juega.">

<!-- Image to display -->
<!-- Replace   «example.com/image01.jpg» with your own -->
<meta property="og:image" content="{{ asset("img/logo.png") }}">

<!-- No need to change anything here -->
<meta property="og:type" content="website" />
<meta property="og:image:type" content="image/png">

<!-- Size of image. Any size up to 300. Anything above 300px will not work in WhatsApp -->
<meta property="og:image:width" content="300">
<meta property="og:image:height" content="300">

<!-- Website to visit when clicked in fb or WhatsApp-->
<meta property="og:url" content="{{ env('APP_URL') }}">
