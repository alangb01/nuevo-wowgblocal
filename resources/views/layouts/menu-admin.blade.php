<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
    <li class="nav-item ">
        <a class="nav-link" href="/home">Inicio <span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="{{ route('intranet.cuentas')}}">Todas las cuentas <span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="{{ route('intranet.personajes')}}">Todos los personajes <span class="sr-only">(current)</span></a>
    </li>

    <li class="nav-item ">
        <a class="nav-link" href="{{ route('intranet.items')}}">Buscar items <span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="{{ route('intranet.correo')}}">Enviar regalos<span class="sr-only">(current)</span></a>
    </li>
</ul>

<ul class="navbar-nav ml-auto mt-2 mt-lg-0">
    <li class="nav-item ">
        <a class="nav-link" href="/">Web <span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item dropdown">
        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            {{ Auth::user()->name }}
        </a>

        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('intranet.logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </div>
    </li>
</ul>
