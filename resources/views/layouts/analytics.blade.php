<!-- Global site tag (gtag.js) - Google Analytics -->
{{--    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-128951021-1"></script>--}}
@if(!env("APP_DEBUG"))
    {{--        <script>--}}
    {{--            window.dataLayer = window.dataLayer || [];--}}
    {{--            function gtag(){dataLayer.push(arguments);}--}}
    {{--            gtag('js', new Date());--}}

    {{--            gtag('config', 'UA-128951021-1');--}}
    {{--        </script>--}}
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-VEXVVBB4NN"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-VEXVVBB4NN');
    </script>
@endif
