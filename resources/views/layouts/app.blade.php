<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    @include('layouts.analytics')
</head>
<body class="">
    <div id="app">
        <div class="text-center my-2 mt-2">
            <img src="{{ asset("img/logo.png") }}" class="img-fluid rounded" alt="...">
        </div>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark ">
           <div class="container ">
               <!-- Navbar content -->
               <a class="navbar-brand" href="#"></a>
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggle" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                   <span class="navbar-toggler-icon"></span>
               </button>

               <div class="collapse navbar-collapse" id="navbarToggle">
                   @auth('admin')
                       @include('layouts.menu-admin')
                   @elseauth()
                       @include('layouts.menu-cuenta')
                   @else
                       <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                           <li class="nav-item ">
                               <a class="nav-link" href="/">Inicio <span class="sr-only">(current)</span></a>
                           </li>
                           <li class="nav-item">
                               <a class="nav-link" href="{{ route('registrarse') }}">Registrarse</a>
                           </li>
                           <li class="nav-item dropdown">
                               <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                   Solicitar accesos
                               </a>
                               <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                   <a class="dropdown-item" href="{{ route('solicitar.usuario') }}">Olvidé mi usuario</a>
                                   <a class="dropdown-item" href="{{ route('solicitar.cambio.clave') }}">Olvidé mi clave</a>
                               </div>
                           </li>
                           <li class="nav-item">
                               <a class="nav-link " href="{{ route('jugar') }}" >Como jugar</a>
                           </li>


                       </ul>

                       <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                           <li class="nav-item ">
                               <a class="nav-link" href="{{ route('login')}}">Iniciar sesión<span class="sr-only">(current)</span></a>
                           </li>
                       </ul>
                   @endauth
               </div>
           </div>
        </nav>
        <div class="container">
            @yield('content')
            <div class="text-center my-2">
                wowgblocal 2020
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>


</body>
</html>
