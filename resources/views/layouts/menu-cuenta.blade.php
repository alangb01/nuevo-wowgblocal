<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
    <li class="nav-item ">
        <a class="nav-link" href="/home">Inicio <span class="sr-only">(current)</span></a>
    </li>
{{--    <li class="nav-item ">--}}
{{--        <a class="nav-link" href="{{ route('cuenta.info')}}">Mi información <span class="sr-only">(current)</span></a>--}}
{{--    </li>--}}
    <li class="nav-item ">
        <a class="nav-link" href="{{ route('cuenta.cuenta')}}">Mi cuenta <span class="sr-only">(current)</span></a>
    </li>
   <li class="nav-item ">
       <a class="nav-link" href="{{ route('cuenta.personajes')}}">Mis personajes <span class="sr-only">(current)</span></a>
   </li>
</ul>

<ul class="navbar-nav ml-auto mt-2 mt-lg-0">
    <li class="nav-item ">
        <a class="nav-link" href="/">Web <span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item dropdown">
        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            {{ Auth::user()->username }}
        </a>

        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </div>
    </li>
</ul>
