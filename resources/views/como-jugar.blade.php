@extends('layouts.app')

@section('content')
        <div class="row">
{{--            <div class="col-lg-6 col-sm-12 mt-2 ">--}}
{{--                @include('_como-jugar.teamspeak')--}}
{{--            </div>--}}

            <div class="col-lg-6 col-sm-12 mt-2 ">
                @include('_como-jugar.instalacion')
            </div>
            <div class="col-lg-6 col-sm-12 mt-2 ">
                @include('_como-jugar.comunicaciones')
            </div>
        </div>
    </div>
@endsection
