<div class="card bg-dark">
    <div class="card-body text-white">
        <h5 class="card-title font-weight-bold text-center">Ultimos conectados</h5>

        <table class="table table-bordered table-dark table-sm mb-0">
            <thead>
            <tr>
                <th scope="col" colspan="3" class="text-center">Nombre</th>
                <th scope="col" class="text-center">Tiempo</th>
            </tr>
            </thead>
            @forelse($personajes_ultima_conexion as $posicion=>$personaje)
                <tr class="{{ $personaje->getBando()==\App\Personaje::ALIANZA?'alianza':'horda' }}">
                    <td class="text-center">{{ $personaje->name  }}</td>
                    <td class="text-center">{{ $personaje->level }}</td>
                    <td class="text-center">
                        <img src="{{ $personaje->getImagenBando() }}" alt="">
                        <img src="{{ $personaje->getImagenClase() }}" alt="">
                        <img src="{{ $personaje->getImagenRaza() }}" alt="">
                    </td>
                    <td class="text-center">
                        {{ $personaje->desconectadoDesde() }}
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="4">No hay personajes recientemente desconectados</td>
                </tr>
                @endforelse
                </tbody>
                <tfoot>
                @if($personajes_ultima_conexion->hasPages())
                    <tr>
                        <td class="text-center" colspan="5">{{ $personajes_ultima_conexion->links('vendor.pagination.simple-minimo') }}</td>
                    </tr>
                @endif
                </tfoot>
        </table>

        {{--                    <a href="#" class="btn btn-primary">Go somewhere</a>--}}
    </div>
</div>
