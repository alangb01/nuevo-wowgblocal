<div class="card bg-dark">
    <div class="card-body text-white">
        <h5 class="card-title font-weight-bold text-center">Ranking de personajes</h5>
        <table class="table table-bordered table-dark table-sm mb-0">
            <thead>
            <tr>
                <th class="text-center">Rank</th>
                <th colspan="3" class="text-center">Personajes</th>
                <th class="text-center">Pts</th>
            </tr>
            </thead>
            <tbody>
            @forelse($personajes_top as $posicion=>$personaje)
                <tr class="{{ $personaje->getBando()==\App\Personaje::ALIANZA?'alianza':'horda' }}">
                    <td class="text-center">{{ $posicion+1+$personajes_top->perPage()*($personajes_top->currentPage()-1) }}</td>
                    <td class="text-center">{{ $personaje->name  }}</td>
                    <td class="text-center">{{ $personaje->level }}</td>
                    <td class="text-center">
                        <img src="{{ $personaje->getImagenBando() }}" alt="">
                        <img src="{{ $personaje->getImagenClase() }}" alt="">
                        <img src="{{ $personaje->getImagenRaza() }}" alt="">
                    </td>
                    <td class="text-right">{{ $personaje->totalKills }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="5">No hay personajes disponibles</td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
            @if($personajes_top->hasPages())
                <tr>
                    <td class="text-center" colspan="5">{{ $personajes_top->links('vendor.pagination.simple-minimo') }}</td>
                </tr>
            @endif
            </tfoot>
        </table>
        {{--                    <a href="#" class="btn btn-primary">Go somewhere</a>--}}
    </div>
</div>
