<?php

use App\Mapa;
use Illuminate\Database\Seeder;

class MapasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \DB::connection('mysql')->table('mapas')->truncate();

        $mapas=$this->getDataMapas();

        foreach($mapas as $mapa){
            $mapa_preparado=[
                'id'        =>  $mapa[0],
                'nombre'    =>  $mapa[1]
            ];
            Mapa::create($mapa_preparado);
        }
    }

    private function getDataMapas(){
        return [
            [0,'Reinos del Este'],
            [1,'Kalimdor'],
            [13,'Testing'],
            [25,'Scott Test'],
            [29,'CashTest'],
            [30,'Valle de Alterac'],
            [33,'Shadowfang Keep'],
            [34,'Stormwind Stockade'],
            [35,'Prision de Ventormenta'],
            [36,'Las Minas de la Muerte'],
            [37,'Azshara Crater'],
            [42,'Collin\'s Test'],
            [43,'Wailing Caverns'],
            [44,'Monasterio'],
            [47,'Razorfen Kraul'],
            [48,'Blackfathom Deeps'],
            [70,'Uldaman'],
            [90,'Gnomeregan'],
            [109,'Sunken Temple'],
            [129,'Razorfen Downs'],
            [169,'Sueño Esmeralda'],
            [189,'Monasterio Escarlata'],
            [209,'Zul\'Farrak'],
            [229,'Blackrock Spire'],
            [230,'Profundidades de Roca Negra'],
            [249,'Guarida de Onyxia'],
            [269,'Cavernas del Tiempo'],
            [289,'Scholomance'],
            [309,'Zul\'Gurub'],
            [329,'Stratholme'],
            [349,'Maraudon'],
            [369,'Deeprun Tram'],
            [389,'Ragefire Chasm'],
            [409,'Nucleo de Magma'],
            [429,'Dire Maul'],
            [449,'Alliance PVP Barracks'],
            [450,'Horde PVP Barracks'],
            [451,'Development Land'],
            [469,'Guarida de Ala Negra'],
            [489,'Warsong Gulch'],
            [509,'Ruins of Ahn\'Qiraj'],
            [529,'Cuenca de Arathi'],
            [530,'Terrallende'],
            [531,'Templo de Ahn\'Qiraj'],
            [532,'Karazhan'],
            [533,'Naxxramas'],
            [534,'La batalla por el monte Hyjal'],
            [540,'Hellfire Citadel: The Shattered Halls'],
            [542,'Hellfire Citadel: The Blood Furnace'],
            [543,'Hellfire Citadel: Ramparts'],
            [544,'Magtheridon\'s Lair'],
            [545,'Coilfang: The Steamvault'],
            [546,'Coilfang: The Underbog'],
            [547,'Coilfang: The Slave Pens'],
            [548,'Coilfang: Serpentshrine Cavern'],
            [550,'Tempest Keep'],
            [552,'Tempest Keep: The Arcatraz'],
            [553,'Tempest Keep: The Botanica'],
            [554,'Tempest Keep: The Mechanar'],
            [555,'Auchindoun: Shadow Labyrinth'],
            [556,'Auchindoun: Sethekk Halls'],
            [557,'Auchindoun: Mana-Tombs'],
            [558,'Auchindoun: Auchenai Crypts'],
            [559,'Arena de Nagrand'],
            [560,'The Escape From Durnholde'],
            [562,'Blade\'s Edge Arena'],
            [564,'Templo Oscuro'],
            [565,'Guarida de Gruul\'s'],
            [566,'El Ojo de la Tormeta'],
            [568,'Zul\'Aman'],
            [571,'Rasganorte'],
            [572,'Ruinas de Lordaeron'],
            [573,'ExteriorTest'],
            [574,'Utgarde Keep'],
            [575,'Pinaculo de Utgarde'],
            [576,'El Nexus'],
            [578,'El Oculus'],
            [580,'The Sunwell'],
            [582,'Transporte: Rut\'theran to Auberdine'],
            [584,'Transporte: Menethil to Theramore'],
            [585,'Magister\'s Terrace'],
            [586,'Transporte: Exodar a Auberdine'],
            [587,'Transporte: Feathermoon Ferry'],
            [588,'Transporte: Menethil a Auberdine'],
            [589,'Transporte: Orgrimmar a Grom\'Gol'],
            [590,'Transporte: Grom\'Gol a Entrañas'],
            [591,'Transporte: Entrañas to Orgrimmar'],
            [592,'Transporte: Borean Tundra Test'],
            [593,'Transporte: Bahia Botin a El Trinquete'],
            [594,'Transporte: Howling Fjord Sister Mercy'],
            [595,'The Culling of Stratholme'],
            [596,'Transport: Naglfar'],
            [597,'Craig Test'],
            [598,'Sunwell Fix'],
            [599,'Halls of Stone'],
            [600,'Drak\'Tharon Keep'],
            [601,'Azjol-Nerub'],
            [602,'Halls of Lightning'],
            [603,'Ulduar'],
            [604,'Gundrak'],
            [605,'Development Land'],
            [606,'QA and DVD'],
            [607,'Strand of the Ancients'],
            [608,'Violet Hold'],
            [609,'Ebon Hold'],
            [610,'Transport: Tirisfal to Vengeance Landing'],
            [612,'Transport: Menethil to Valgarde'],
            [613,'Transport: Orgrimmar to Warsong Hold'],
            [614,'Transport: Stormwind to Valiance Keep'],
            [615,'The Obsidian Sanctum'],
            [616,'The Eye of Eternity'],
            [617,'Dalaran Sewers'],
            [618,'The Ring of Valor'],
            [619,'Ahn\'kahet: The Old Kingdom'],
            [620,'Transport: Moa\'ki to Unu\'pe'],
            [621,'Transport: Moa\'ki to Kamagua'],
            [622,'Transport: Orgrim\'s Hammer'],
            [623,'Transport: The Skybreaker'],
            [624,'Vault of Archavon']
        ];
    }
}
